<?php
/**
 * Bootstrap file
 */

//Load core
require_once ('core/model.php');
require_once ('core/view.php');
require_once ('core/controller.php');

$url = explode('/', $_SERVER['REQUEST_URI']);

$controller =  (
                isset($url[1]) &&
                file_exists('controllers/'.$controller.".php")
                )
                ? $url[1] : "Main";

$method =   isset($url[2]) ? $url[2] : "index";

//Load main controller
require_once('controllers/'.$controller.'.php');
require_once('models/'.$controller.'.php');

//Execute default controller
$appName = $controller.'Controller';
$methodName = $method."Action";
$app = new $appName;
$app->{$methodName}();
