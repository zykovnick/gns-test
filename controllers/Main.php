<?php

class MainController extends Controller{


    public function __construct(){
        parent::__construct();
        $this->model = new MainModel();
    }

    public function indexAction(){

        $data['films'] = $this->model->getFilms();
        $this->view->render('Main', $data);
    }

    public function addAction(){

        $this->view->render('AddForm');
    }

    public function saveAction(){

        $filmName = isset($_POST['filmName']) ? $_POST['filmName'] : null;
        $filmYear = isset($_POST['filmYear']) ? $_POST['filmYear'] : null;
        $isActive = isset($_POST['isActive']) ? 1 : 0;

        if(!$filmName OR !$filmYear) {
            $this->view->render('AddFormMistake');
            exit;
        }

        $save = $this->model->addFilm($filmName, $filmYear, $isActive);

        if($save){

            header('Location: ../main');
        }else{
            $this->view->render('AddFormMistake');
        }

    }

}