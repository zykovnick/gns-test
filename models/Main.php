<?php
/**
 * Class MainModel
 */

class MainModel extends Model{

    public function getFilms(){

        $dbResult = $this->DBConnection->query("SELECT name, year FROM Films WHERE isActive = 1");

        while($row = $dbResult->fetch_array()){
            $rows[] = $row;
        }

        return $rows;
    }

    public function addFilm($name, $year, $isActive){

        $stmt = $this->DBConnection->prepare("INSERT INTO Films (id, name, year, isActive) VALUES ('', ?, ?, ?)");
        $stmt->bind_param("ssi",$name, $year, $isActive);
        $result = $stmt->execute();

        return $result;
    }

}