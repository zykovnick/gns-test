<!DOCTYPE html>
<html lang="en">
<head>
    <title>Active films</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Some awesome active films</h2>
    <p class="navbar-btn">
        <a href="/main/add" class="btn btn-default">Add your own film</a>
    </p>
    <table class="table">
        <thead>
        <tr>
            <th>Name</th>
            <th>Year</th>
        </tr>
        </thead>
        <tbody>
        <? foreach($films as $film) { ?>
        <tr>
            <td><? echo $film['name'] ?></td>
            <td><? echo $film['year'] ?></td>
        </tr>
        <? } ?>
        </tbody>
    </table>
</div>

</body>
</html>
