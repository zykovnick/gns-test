<!DOCTYPE html>
<html lang="en">
<head>
    <title>Add film</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Add film</h2>
    <form action="../main/save" method="post">
        <div class="form-group">
            <input type="text" class="form-control"  placeholder="Film name" name="filmName">
        </div>
        <div class="form-group">
            <select class="form-control" name="filmYear">
                <? for($i = 1900; $i <= date("Y"); $i++) { ?>
                <option><? echo $i; ?></option>
                <? } ?>
            </select>
        </div>
        <div class="checkbox">
            <label><input type="checkbox" name="isActive"> Active</label>
        </div>
        <button type="submit" class="btn btn-primary">Add</button>
    </form>
</div>

</body>
</html>
