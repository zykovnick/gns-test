<?php

/**
 * Core Controller
 *
 * Class Controller
 */
class Controller {
	
	public $model;
	public $view;

    /**
     * Initialize view
     */
    public function __construct(){
		$this->view = new View();
	}

}
