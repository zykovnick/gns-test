<?php

/**
 * Data presentation
 * Class View
 */
class View{

    /**
     * Render data
     * @param $view - Path to view
     * @param null $data
     */
    function render($view , $data = null){

        //Transform array to variables
        if(is_array($data)) {
            extract($data);
        }
        //Load view
        include 'views/'. $view.'.php';
	}
}
