<?php

/**
 * Data procession
 * Class Model
 */

class Model{

    protected $DBConnection;
    protected $config = [];

    public function __construct(){

        // Yes, it's terrible to define it here
        $this->config['dbHost'] = "localhost";
        $this->config['dbUser'] = "root";
        $this->config['dbPass'] = "";
        $this->config['dbName'] = "futo";

        $this->connectDB();

    }

    public function connectDB(){

        try {

            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
            $this->DBConnection = new mysqli(
                                        $this->config['dbHost'],
                                        $this->config['dbUser'],
                                        $this->config['dbPass'],
                                        $this->config['dbName']
                                    );

            $this->DBConnection->set_charset("utf8");

        } catch (Exception $e) {
            echo "Service unavailable" . PHP_EOL;
            echo "Message: " . $e->getMessage();
            exit;
        }
    }

}